import pchange

fname = input("Entrr file name: ")
fhand = open(fname)
times = {}
for line in fhand:
    if len(line) > 2 and line.startswith("From "):
        words = line.split()
        time = words[5]
        time = time.split(":") 
        hour = time[0]
        if hour not in times:
            times[hour] = 1
        else:
            times[hour] += 1

lst = []

for key,val in list(times.items()):
    lst.append((key,val))

lst.sort(reverse=False)

for key,val in lst:
    print(key,val)