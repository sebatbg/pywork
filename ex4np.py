import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup

fname = input("Enter web address: ")
html = urllib.request.urlopen(fname)
par_count = 0

soup = BeautifulSoup(html, 'html.parser')

tags = soup('p')

for tag in tags:
    print(tag.contents)
    par_count += 1

print("Znacznikow p jest", par_count)
