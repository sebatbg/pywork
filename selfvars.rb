class C
  puts "Wlasnie aktywowano blok definicji klasy. Oto obiekt self: "
  p self
  @v = "Jestem zmienna instancji na najwyzszym poziomie zawartosci klasy."
  puts "Oto zmienna instancji @v ktora nalezy do obiektu #{self}"
  p @v
  def show_var
    puts "Wlasnie aktywowano blok definicji metody instancji. Oto obiekt self:"
    p self
    puts "Oto zmienna instancji @v ktora nalezy do obiektu #{self}"
    p @v
  end
end