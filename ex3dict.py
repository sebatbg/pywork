import pchange

fname = input("Enter file name: ")
fhand = open(fname)
sl = {}

for line in fhand:
    if len(line) > 2 and line.startswith("From "):
        words = line.split()
        email = words[1]
        if email not in sl:
            sl[email] = 1
        else:
            sl[email] +=1

print(sl)

biggest = None
user = None
for key ,value in list(sl.items()):
    if biggest is None or biggest < value:
        biggest = value
        user = key

print("*" * 20)

print(user, biggest)