import pchange


fname = input("Enter file name: ")
fhand = open(fname)

messages = {}
for line in fhand:
    if len(line) > 2 and line.startswith("From "):
        words = line.split()
        message = words[1]
        if message not in messages:
            messages[message] = 1
        else:
            messages[message] += 1

print(messages)