import pchange
fname = input("Enter file name: ")
fhand = open(fname)

address = {}

for line in fhand:
    if len(line) > 2 and line.startswith("From "):
        words = line.split()
        addr = words[1]
        if addr not in address:
            address[addr] = 1
        else:
            address[addr] += 1

#print(address)

lst = list()
for key ,val in list(address.items()):
    lst.append((val,key))

lst.sort(reverse=True)

for key,val in lst[:1]:
    print(val,key)