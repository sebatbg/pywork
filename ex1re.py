import pchange
import re

count = 0
fhand = open("mbox.txt")
exp = input("Enter a regular expression: ")

for line in fhand:
    line = line.strip()
    x = re.findall(exp,line)
    if len(x) > 0:
        count += 1

print("mbox.txt had {} lines that matched {}".format(count,exp))