import pchange
import re

fname = input("Enter file name: ")
fhand = open(fname)

l = []
count = 0
for line in fhand:
    line = line.rstrip()
    x = re.findall("New Revision: ([0-9.]+)",line)
    if len(x) > 0:
        for elem in x:
            l.append(float(elem))
            count +=1

print(sum(l) / count)