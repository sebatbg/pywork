class C
  def x(value_for_a, recurse=false)
    a = value_for_a
    print "Lancuch inspekcji dla obiektu self:"
    p self
    puts "Wartosc zmiennej a:"
    puts a
    if recurse
      puts "Wywolanie samej siebie (rekurencja)"
      x("Druga wartosc zmiennej a")
      puts "Powrot po rekurencji. Wartosc zmiennej a:"
      puts a
    end
  end
end