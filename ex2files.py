fname = input('Enter file name: ')
fhand = open(fname)
count = 0
total = 0
for line in fhand:
    if line.startswith('X-DSPAM-Confidence'):
        count = count + 1
        start = line.find(':')
        end = line.find("''", start)
        numbers = line[start+1:end]
        numbers = float(numbers)
        total = total + numbers
        
avc = total / count
print('Average spam confidence: ', avc)

