import pchange

fname = input("Enter file name: ")
days = {}
fhand = open(fname)
for line in fhand:
    if len(line) > 2 and line.startswith("From "):
        words = line.split()
        day = words[2]
        if day not in days:
            days[day] = 1
        else:
            days[day] += 1
            
print(days)
        