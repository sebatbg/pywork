puts "Najwyzszy poziom"
puts "Obiekt self to #{self}"

class C
  puts "Blok definicji klasy"
  puts "Obiekt self to #{self}"
  def self.x
    puts "Metoda klasy C.x"
    puts "Obiekt self to #{self}"
  end
  def m
    puts "Metoda instancji C#m"
    puts "Obiekt self to #{self}"
  end
end