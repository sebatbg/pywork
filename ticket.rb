ticket = Object.new

def ticket.date
  "01/02/03"
end

def ticket.venue
  "Sala miejska"
end

def ticket.event
  "Wieczor literacki autora"
end

def ticket.performer
  "Mark Twain"
end

def ticket.seat
  "Balkon drugi, rzad J, miejsce 12"
end


def ticket.price
  5.50
end

puts "Bilet na wydarzenie: #{ticket.event}. Miejsce: #{ticket.venue}." +
"Wykonawca: #{ticket.performer}." +
"Miejsce: #{ticket.seat}." +
"Cena: $#{"%.2f." % ticket.price}"

if ticket.respond_to?(request)
  ticket.send(request)
else
  puts "Takie informacje nie sa dostepne"
